package com.vassago.CC;

public class Classes implements CaseItem{
	private boolean recurs;
	private String date,location;
	
	public Classes(String date, String location, boolean recurs){
		this.date=date;
		this.recurs=recurs;
		this.location=location;
	}
	
	public String getDate() {
		return date;
	}

	public boolean isRecurring() {
		return recurs;
	}
	
	public String getLocation(){
		return location;
	}
	
	public String toString(){
		StringBuilder sb=new StringBuilder();
		
		return sb.toString();
	}

}
