package com.vassago.CC;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

public class EditCase extends Activity{
	private final int DEBUG=0,DIALOG_CASE_NULL=1,DIALOG_DATE=2;
	private String selectedPerson,caseDesc,caseNum,caseDate;
	private int mYear,mMonth,mDay,id;
	private Button caseSelectButton;
	
	/** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_case);
        Bundle extras=getIntent().getExtras();
        caseNum=extras.getString("caseNum");
        caseDesc=extras.getString("desc");
        caseDate=extras.getString("date");
        id=extras.getInt("id");
        parseDate();
        //Gets and sets button for case date entry. Defaults zero for m/d/y
        caseSelectButton=(Button)findViewById(R.id.editCaseDate);
        caseSelectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DIALOG_DATE);
            }
        });
        Calendar c=Calendar.getInstance();
        mYear=c.get(Calendar.YEAR);
        mMonth=c.get(Calendar.MONTH);
        mDay=c.get(Calendar.DAY_OF_MONTH);
        //Set the text in the EditText fields
        EditText caseNumET=(EditText)findViewById(R.id.editCaseNumField);
        EditText caseDescET=(EditText)findViewById(R.id.editCaseDescField);
        caseNumET.setText((CharSequence)caseNum);
        caseDescET.setText((CharSequence)caseDesc);
    }
    
	public void updateCase(View button){
		CCHelper cch=CCHelper.getCCH();
		EditText cNum = (EditText) findViewById(R.id.editCaseNumField);  
		EditText cDesc = (EditText) findViewById(R.id.editCaseDescField);
		caseNum= cNum.getText().toString();  
		caseDesc = cDesc.getText().toString();
		if(caseNum.equals("")||caseDesc.equals("")){
			showDialog(DIALOG_CASE_NULL);
			setContentView(R.layout.create_case);
			return;
		}
		//showDialog(DEBUG);
		StringBuilder date=new StringBuilder();
		date.append((mMonth+1)).append("/").append(mDay).append("/").append(mYear);
		cch.insertCase(caseNum, date.toString(), caseDesc, id);
		Bundle bundle=new Bundle();
		bundle.putString(CCHelper.C_CASE, caseNum);
		bundle.putString(CCHelper.C_DESC, caseDesc);
		bundle.putString(CCHelper.C_DATE, date.toString());
		Intent i = new Intent();
		i.putExtras(bundle);
		setResult(RESULT_OK, i);
		finish();
	}
    
    private void parseDate(){
		Pattern p=Pattern.compile("/");
		Matcher m=p.matcher(caseDate);
		int tempBeg=0,tempEnd=0;
		m.find();
		mMonth=Integer.parseInt(caseDate.substring(tempBeg, (tempEnd=m.end()-1)));
		tempBeg=tempEnd;
		m.find();
		mDay=Integer.parseInt(caseDate.substring(tempBeg+1, (tempEnd=m.end()-1)));
		mYear=Integer.parseInt(caseDate.substring(tempEnd+1, caseDate.length()));
    }
    
	/**
	 * Shows a dialog. One for if a first and last name isn't entered.
	 * Also uses dialog for debugging.
	 */
    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        AlertDialog.Builder builder;
        switch(id) {
        case DIALOG_CASE_NULL:
        	builder = new AlertDialog.Builder(this);
        	builder.setMessage("Please enter a case number, description, and date.")
        	       .setCancelable(false)
        	       .setNeutralButton("Close", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                dialog.cancel();
        	           }
        	       });
        	dialog = builder.create();
            break;
        case DIALOG_DATE:
        	return new DatePickerDialog(this,mDateSetListener,mYear, mMonth, mDay);
        case DEBUG:
        	builder = new AlertDialog.Builder(this);
        	builder.setMessage(selectedPerson)
        	       .setCancelable(false)
        	       .setNeutralButton("Close", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                dialog.cancel();
        	           }
        	       });
        	dialog = builder.create();
        	break;
        default:
            dialog = null;
        }
        return dialog;
    }
    
    /**
     * Override the back button functionality. Sets it to menu.
     */
    public void onBackPressed()  
    {         
    	this.startActivity(new Intent(this,CCActivity.class));  
    }
    
    // the callback received when the user sets the date in the dialog
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, 
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                }
          };
}
