package com.vassago.CC;

import java.util.ArrayList;

public class Case {
	private String description,date,caseNum;
	private ArrayList<CaseItem> caseItems;
	
	public Case(String caseNum,String description,String date){
		this.caseNum=caseNum;
		this.description=description;
		this.date=date;
		caseItems=new ArrayList<CaseItem>();
	}
	
	public String getCaseNum(){
		return caseNum;
	}
	
	public String getDesc(){
		return description;
	}
	
	public String getDate(){
		return date;
	}
	
	public String toString(){
		return "Case #: "+caseNum+"\nCase Description: "+description+"\nCase Date: "+date;
	}
	
	public ArrayList<CaseItem> getCaseItems(){
		return caseItems;
	}
	
	public void addCaseItem(CaseItem ci){
		caseItems.add(ci);
	}
}
