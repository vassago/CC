package com.vassago.CC;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

public class CreatePerson extends Activity{
	private final int DEBUG=0,DIALOG_NAME_NULL=1,DIALOG_DOB=2;
	private String lName,fName,state;
    private int mYear,mMonth,mDay;
	private Button dobSelectButton;
    
	/** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_person);

        //Gets and sets button for dob entry.
        dobSelectButton=(Button)findViewById(R.id.setDOB);
        dobSelectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DIALOG_DOB);
            }
        });
        Calendar c=Calendar.getInstance();
        mYear=c.get(Calendar.YEAR)-25;
        mMonth=1;
        mDay=0;
        //Creates the state select spinner
        Spinner spinner = (Spinner) findViewById(R.id.state_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.states_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new ItemSelectedListener());
    }
	/**
	 * Shows a dialog. One for if a first and last name isn't entered.
	 * Also uses dialog for debugging.
	 */
    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        AlertDialog.Builder builder;
        switch(id) {
        case DIALOG_NAME_NULL:
        	builder = new AlertDialog.Builder(this);
        	builder.setMessage("Please enter a first and last name.")
        	       .setCancelable(false)
        	       .setNeutralButton("Close", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                dialog.cancel();
        	           }
        	       });
        	dialog = builder.create();
            break;
        case DIALOG_DOB:
        	return new DatePickerDialog(this,mDateSetListener,mYear, mMonth, mDay);
        case DEBUG:
        	builder = new AlertDialog.Builder(this);
        	builder.setMessage("fName: "+fName+"\tlName: "+lName)
        	       .setCancelable(false)
        	       .setNeutralButton("Close", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                dialog.cancel();
        	           }
        	       });
        	dialog = builder.create();
        	break;
        default:
            dialog = null;
        }
        return dialog;
    }
	
	/**
	 * Puts entered data into a bundle to be returned to the calling process
	 * @param button
	 */
	public void createPerson(View button){
		EditText firstName = (EditText) findViewById(R.id.firstNameField);  
		EditText lastName = (EditText) findViewById(R.id.lastNameField);
		lName= lastName.getText().toString();  
		fName = firstName.getText().toString();
		if(fName.equals("")||lName.equals("")){
			showDialog(DIALOG_NAME_NULL);
			setContentView(R.layout.create_person);
			return;
		}
		if(state.equals("Select your state")){
			state="";
		}
		Bundle bundle=new Bundle();
		bundle.putString(CCHelper.PT_FNAME, fName);
		bundle.putString(CCHelper.PT_LNAME, lName);
		bundle.putString(CCHelper.PT_STATE, state);
		Intent i = new Intent();
		i.putExtras(bundle);
		setResult(RESULT_OK, i);
		finish();
	}
	/**
	 * For getting the selection of the spinner.
	 */
	public class ItemSelectedListener implements OnItemSelectedListener {
	    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
	    	state= parent.getItemAtPosition(pos).toString();
	    }
	    public void onNothingSelected(AdapterView<?> parent) {
	      // Do nothing.
	    }
	}
    /**
     * Override the back button functionality. Sets it to menu.
     */
    public void onBackPressed()  
    {         
    	startActivity(new Intent(this,CCActivity.class));  
    } 
    // the callback received when the user sets the date in the dialog
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, 
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                }
          };
}
