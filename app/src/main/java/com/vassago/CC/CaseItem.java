package com.vassago.CC;

public interface CaseItem {
	String getDate();
	boolean isRecurring();
	String getLocation();
}
