package com.vassago.CC;

import java.util.ArrayList;

public class Person {
	private String fName,lName;
	private int id;
	private ArrayList<Case> cases;
	
	public Person(String fName, String lName, int id){
		cases=new ArrayList<Case>();
		this.fName=fName;
		this.lName=lName;
		this.id=id;
	}
	
	public String toString(){
		return fName+" "+lName;
	}
	
	protected ArrayList<Case> getCases(){
		return cases;
	}
	
	protected void clearCases(){
		cases.clear();
	}
	
	protected void addCase(String cNum, String desc,String date){
		cases.add(new Case(cNum,desc,date));
	}
	
	protected Case getCase(int id){
		return cases.get(id);
	}
	
	protected String getFName(){
		return fName;
	}
	
	protected String getLName(){
		return lName;
	}
	
	protected int getID(){
		return id;
	}
}
