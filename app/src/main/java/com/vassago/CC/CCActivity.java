package com.vassago.CC;

import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * 
 * @author Richard Thatcher
 *
 */
public class CCActivity extends Activity {
	static final String DATABASE_NAME = "CC.db";
	private final int CREATE_PERSON=0,CASE_LIST=1,DEBUG=666;
	private ArrayList<Person> people;
	private CCHelper cch;
	private Button caseList,stats,goals;
	private String fname,lname;
	
	/** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cch=CCHelper.init(this);
        
        
        setContentView(R.layout.main);
        if(loadList()){
        	caseList=(Button)findViewById(R.id.Cases);
        	caseList.setEnabled(false);
        	stats=(Button)findViewById(R.id.Stats);
        	stats.setEnabled(false);
        	goals=(Button)findViewById(R.id.Goals);
        	goals.setEnabled(false);
        }
    }
    
    /** Called when activity is ended. */
    protected void onDestroy(){
    	super.onDestroy();
    	cch.close();
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	Bundle extras = data.getExtras();

    	switch(requestCode) {
    	case CREATE_PERSON:
    	    String fName = extras.getString(CCHelper.PT_FNAME);
    	    String lName = extras.getString(CCHelper.PT_LNAME);
        	fname=fName;
        	lname=lName;
        	for(Person p:people){
        		if(p.toString().equals(fName+" "+lName))return;
        	}
        	//showDialog(DEBUG);
        	if(cch.insertPerson(fName,lName)==-1){
        		Log.e("CC","DB insert Error, Person Table");
        	}
    	    int pid=cch.getPID(fName, lName);
    	    if(people.size()==0){
    	    	caseList.setEnabled(true);
            	stats.setEnabled(true);
            	goals.setEnabled(true);
    	    }
        	people.add(new Person(fName,lName,pid));
    	    break;
    	case CASE_LIST:
    		break;
    	}
    }
    
    /**
     * Tests the DB
     * @param view TextView to write to in order to print results
     */
    private void DBTest(TextView view){
    	if(cch.insertPerson("john","doe")==-1){
    		Log.e("CC","DB insert Error, Person Table");
    	}
    	
    	Cursor cur=cch.queryPeople();
    	cur.moveToLast();
    	
    	if(cch.insertCase("12666", "05-13-1999", "world domination",cur.getInt(2))==-1){
    		Log.e("CC","DB insert Error, Cases Table");
    	}
    	cur=cch.queryPeople();
    	
    	
    	//cur=cch.queryCases(0);
        cur.moveToFirst();
        while (cur.isAfterLast() == false) {
            view.append("\n" + cur.getString(1)+", "+cur.getString(0)+" ID: "+cur.getString(2));
       	    cur.moveToNext();
        }
        cur.close();
    }
    
    /**
     * Create new person object
     */
    public void createPerson(View target){
    	Intent i=new Intent(this,CreatePerson.class);
    	startActivityForResult(i,CREATE_PERSON);
    }
    
    public void cases(View target){
    	Intent i=new Intent(this,CaseList.class);
    	startActivityForResult(i,CASE_LIST);
    }
    
    public void stats(View target){
    	
    }
    
    
    public void goals(View target){
    	
    }
    
    private boolean loadList(){
    	boolean ret=false;
    	people=new ArrayList<Person>();
    	Cursor cur=cch.queryPeople();

        if(!cur.moveToFirst())
        	return true;
        while (cur.isAfterLast() == false) {
            ret=false;
        	people.add(new Person(cur.getString(0),cur.getString(1),Integer.parseInt(cur.getString(2))));
       	    cur.moveToNext();
        }
        cur.close();
        return ret;
    }
    
	/**
	 * For debugging.
	 */
    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        AlertDialog.Builder builder;
        switch(id) {
        case DEBUG:
        	builder = new AlertDialog.Builder(this);
        	builder.setMessage("fName: "+fname+"\tlName: "+lname)
        	       .setCancelable(false)
        	       .setNeutralButton("Close", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                dialog.cancel();
        	           }
        	       });
        	dialog = builder.create();
        	break;
        default:
            dialog = null;
        }
        return dialog;
    }
    /**
     * Override the back button functionality. Sets it to menu.
     */
    public void onBackPressed()  
    {         
    	this.startActivity(new Intent(this,CCActivity.class));  
    } 
}
