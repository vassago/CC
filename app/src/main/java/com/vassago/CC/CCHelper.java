package com.vassago.CC;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * 
 * @author Richard Thatcher
 *
 */
public class CCHelper extends Application{
    private static CCHelper cch;
	
	private static final int DATABASE_VERSION = 12;
    
    protected static final String PT_FNAME="FName",PT_LNAME="LName",PT_STATE="State",PT_DOB="dob";
    protected static final String C_DATE="CaseDate",C_DESC="CaseDesc",C_CASE="CaseNum", C_ID="CPID";
    
    private static final String CC_PT_NAME = "Person",CC_CT_NAME="Cases",PT_ID="PID";
    private static final String PT_TABLE_CREATE =
	        	"CREATE TABLE " + CC_PT_NAME + " (" +
	        	PT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
	            PT_FNAME + " TEXT not null, " +
	            PT_LNAME + " TEXT not null, "+
	            PT_DOB + " TEXT, " +
	            PT_STATE + " TEXT" +
	            ");";
    private static final String C_TABLE_CREATE =
	            "CREATE TABLE " + CC_CT_NAME + " (" +
	            C_CASE + " TEXT UNIQUE PRIMARY KEY, " +
	            C_DATE + " TEXT, " +
	            C_DESC + " TEXT, " +
	            C_ID + " INTEGER not null, " +
	            "FOREIGN KEY (" + C_ID + ") REFERENCES " + CC_PT_NAME + " (" + PT_ID + ") ON DELETE CASCADE" +
	            ");";
    private DBOpenHelper DBOH;
    private SQLiteDatabase db;
    private Context ctx;
    
    private CCHelper(Context ctx){
    	this.ctx=ctx;
    	DBOH=new DBOpenHelper(this.ctx);
    	db=DBOH.getWritableDatabase();
    }
    
    protected static CCHelper init(Context ctx){
    	cch=new CCHelper(ctx);
    	return cch;
    }
    
    protected static CCHelper getCCH(){
    	return cch;
    }
	
    /**
     * Insert into the Cases table
     * @param caseNum
     * @param date
     * @param desc
     * @return
     */
    public long insertCase(String caseNum,String date, String desc,int currID){
    	ContentValues cv=new ContentValues();
    	cv.put(C_CASE, caseNum);
    	cv.put(C_DATE, date);
    	cv.put(C_DESC, desc);
    	cv.put(C_ID, currID);
    	return db.insert(CC_CT_NAME, null, cv);
    }
    
    /**
     * UNTESTED!!!!!
     * @param caseNum Updated case number
     * @param date Updated date
     * @param desc Updated description
     * @param oldCaseNum Old case number for case number changes
     * @param currID ID of person
     */
    public void updateCase(String caseNum,String date, String desc, String oldCaseNum,int currID){
    	ContentValues cv=new ContentValues();
    	cv.put(C_DATE, date);
    	cv.put(C_DESC, desc);
    	cv.put(C_ID, currID);
    	db.update(CC_CT_NAME, cv, C_CASE+"=\""+oldCaseNum+"\"", null);
    }
    
    /**
     * Insert into the Persons table
     * @param fName
     * @param lName
     * @param id
     * @return
     */
    public long insertPerson(String fName,String lName){
    	ContentValues cv=new ContentValues();
    	cv.put(PT_FNAME,fName);
    	cv.put(PT_LNAME,lName);
    	return db.insert(CC_PT_NAME, null, cv);
    }
    
    //TODO: Insert person with dob. Did not add with state because will want to allow user
    // to change/add these values later
    public long insertPerson(String fName,String lName, String dob){
    	ContentValues cv=new ContentValues();
    	cv.put(PT_FNAME,fName);
    	cv.put(PT_LNAME,lName);
    	cv.put(PT_DOB, dob);
    	return db.insert(CC_PT_NAME, null, cv);
    }
    
    public int getPID(String fName,String lName){
    	Cursor cur=db.query(CC_PT_NAME, new String[] {PT_ID}, PT_FNAME+"=\""+fName+"\" and "+PT_LNAME+"=\""+lName+"\"", null, null, null, null);
    	if(!cur.moveToFirst()){
    		return -1;
    	}
    	int ID=Integer.parseInt(cur.getString(0));
    	cur.close();
    	return ID;
    }
    
    /**
     * Deletes the database entry with the names passed in 
     * @param fName First name to search db for
     * @param lName Last name to search db for
     * @return Number of rows deleted
     */
    public int deletePerson(String fName,String lName){
    	return db.delete(CC_PT_NAME, PT_FNAME+"=\""+fName+"\" and "+PT_LNAME+"=\""+lName+"\"", null);
    }
    
    public int deleteCase(String caseNum){
    	return db.delete(CC_CT_NAME, C_CASE+"=\""+caseNum+"\"", null);
    }
    
    /**
     * 
     * @return Name of person DB
     */
    public String getPersonDBName(){
    	return CC_PT_NAME;
    }
    
    /**
     * 
     * @return Name of cases DB
     */
    public String getCasesDBName(){
    	return CC_CT_NAME;
    }
    
    /**
     * 
     * @return Cursor of all people in DB
     */
    public Cursor queryPeople(){
    	return db.query(CC_PT_NAME, new String[] {PT_FNAME,PT_LNAME,PT_ID}, null, null, null, null, null);
    }
    
    /**
     * 
     * @param pid ID of person to get cases for
     * @return Cursor of cases
     */
    public Cursor queryCases(int pid){
    	return db.query(CC_CT_NAME+","+CC_PT_NAME, new String[] {C_CASE,C_DESC,C_DATE}, PT_ID+"="+C_ID, null, null, null, null);
    }
    
    /**
     * 
     * @param pid ID of person to get cases for
     * @param caseNum Case num to return;
     * @return Cursor of cases
     */
    public Cursor getCase(int pid,String caseNum){
    	return db.query(CC_CT_NAME+","+CC_PT_NAME, new String[] {C_CASE,C_DESC,C_DATE}, PT_ID+"="+C_ID+" and "+caseNum+"="+C_CASE, null, null, null, null);
    }
    
    public void close(){
    	DBOH.close();
    }
	
    /**
     * Database helper. Manages creating, opening, and upgrading DB.
     * @author Richard Thatcher
     *
     */
    public class DBOpenHelper extends SQLiteOpenHelper {
    	
        DBOpenHelper(Context context) {
            super(context, CCActivity.DATABASE_NAME, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("PRAGMA foreign_keys=ON;");
            db.execSQL(PT_TABLE_CREATE);
            db.execSQL(C_TABLE_CREATE);
        }
        
        public void onOpen(SQLiteDatabase db){
        	super.onOpen(db);
        	db.execSQL("PRAGMA foreign_keys=ON;");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            // Logs that the database is being upgraded
            Log.w("CC", "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");

            // Kills the table and existing data
            db.execSQL("DROP TABLE IF EXISTS Cases");
            db.execSQL("DROP TABLE IF EXISTS Person");  
            // Recreates the database with a new version
            onCreate(db);
        }
    }

}
