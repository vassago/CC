package com.vassago.CC;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

public class CaseList extends Activity{

	private final int DEBUG=0,DIALOG_SELECT_PERSON=1,CREATE_CASE=2,
			DIALOG_SELECT_CASE=3,EDIT_CASE=4,DIALOG_SELECT_REMINDER=5,SELECT_DATE=6,
			SELECT_TIME=7,TV_NUM=255;
	private final int SET_REMINDER=892,SELECT_EDIT_CASE=893,SELECT_DELETE_CASE=894;
	protected final int COURSES=128,COURT_DATE=129,PO_MEETING=130,PUBLIC_SERVICE=131,PAYMENTS=132;
	private int mYear, mMonth,mDay,mHour,mMinute,mAMPM,sItem;
	private String selectedName;
	private Person selectedPerson;
	private ArrayList<Person> people;
	private ArrayList<String> names;
	private CCHelper cch;
	private Case selectedCase;
	private Activity tempActivity;
	
	/** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.case_list);
        cch=CCHelper.getCCH();
        loadList();
        showDialog(DIALOG_SELECT_PERSON);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	Bundle extras;
    	switch(requestCode) {
    	case CREATE_CASE:
    		extras=data.getExtras();
    		selectedPerson.addCase(extras.getString(CCHelper.C_CASE), extras.getString(CCHelper.C_DESC), extras.getString(CCHelper.C_DATE));
    		updateView();
    		break;
    	case EDIT_CASE:
    		extras=data.getExtras();
    		String caseNum=extras.getString(CCHelper.C_CASE);
    		if(caseNum.equals(selectedCase.getCaseNum())){
    			cch.updateCase(caseNum, extras.getString(CCHelper.C_DATE), extras.getString(CCHelper.C_DESC), selectedCase.getCaseNum(), selectedPerson.getID());
    		}else{
    			cch.deleteCase(selectedCase.getCaseNum());
    			cch.insertCase(caseNum, extras.getString(CCHelper.C_DATE), extras.getString(CCHelper.C_DESC), selectedPerson.getID());
    		}
    		loadCases();
    		updateView();
    		break;
    	}
    }

    
    private void updateView(){
    	RelativeLayout rl=(RelativeLayout)findViewById(R.id.caseListLayout);
    	int tvNum=TV_NUM;
    	TextView tv;
    	boolean addView=false;
        for(Case c:selectedPerson.getCases()){
        	if((tv=(TextView)findViewById(tvNum))==null){
        		tv=new TextView(this);
        		addView=true;
        	}
        	StringBuilder sb=new StringBuilder();
        	sb.append(c.toString());
        	sb.append("\n\n");
        	tv.setText((CharSequence)sb.toString());
            tv.setMovementMethod(new ScrollingMovementMethod());
            tv.setTextSize(25);
            tv.setTextColor(Color.BLACK);
            tv.setGravity(Gravity.CENTER_HORIZONTAL);
            tv.setId(tvNum);
            RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            if(tvNum!=255)lp.addRule( RelativeLayout.BELOW, tvNum-1);
            tv.setLayoutParams(lp);
            tv.setOnClickListener(new View.OnClickListener(){
            	public void onClick(View v){
            		TextView tv0=(TextView)v;
            		selectedCase=selectedPerson.getCase((tv0.getId()-TV_NUM));
            		showDialog(DIALOG_SELECT_CASE);
            	}
            });
            if(addView)rl.addView(tv);
            tvNum++;
        }
        while((tv=(TextView)findViewById(tvNum))!=null){
    		rl.removeView(tv);
    		tvNum++;
    	}
    }
    
    /**
     * Create new cases object
     */
    public void createCase(View target){
    	Intent i=new Intent(this,CreateCase.class);
    	Bundle bundle=new Bundle();
    	bundle.putString("Name", selectedName);
    	bundle.putInt("id", selectedPerson.getID());
    	i.putExtras(bundle);
    	startActivityForResult(i,CREATE_CASE);
    }
    
    private void loadList(){
    	people=new ArrayList<Person>();
    	names=new ArrayList<String>();
    	Cursor cur=cch.queryPeople();
        cur.moveToFirst();
        while (cur.isAfterLast() == false) {
        	String fName=cur.getString(0);
        	String lName=cur.getString(1);
        	int id=Integer.parseInt(cur.getString(2));
        	people.add(new Person(fName,lName,id));
        	names.add(fName+" "+lName);
       	    cur.moveToNext();
        }
        cur.close();
    }
    
    private void loadCases(){
    	selectedPerson.clearCases();
    	Cursor cur=cch.queryCases(selectedPerson.getID());
        cur.moveToFirst();
        while (cur.isAfterLast() == false) {
        	String caseNum=cur.getString(0);
        	String caseDesc=cur.getString(1);
        	String caseDate=cur.getString(2);
        	selectedPerson.addCase(caseNum, caseDesc, caseDate);
       	    cur.moveToNext();
        }
        cur.close();
    }
    
    private void insertCalendar(){
    	Calendar cal = Calendar.getInstance();              
    	int tempHour=mHour;
    	if(mAMPM==1){
    		tempHour+=12;
    	}
    	String[] temp=getResources().getStringArray(R.array.reminder_types);
    	cal.set(mYear, mMonth, mDay, tempHour, mMinute);
    	Intent intent = new Intent(Intent.ACTION_EDIT);
    	intent.setType("vnd.android.cursor.item/event");
    	intent.putExtra("beginTime", cal.getTimeInMillis());
    	intent.putExtra("allDay", false);
    	//intent.putExtra("rrule", "FREQ=");
    	intent.putExtra("endTime", cal.getTimeInMillis()+60*60*1000);
    	intent.putExtra("title", temp[sItem]);
    	startActivity(intent);
    }
    
	/**
	 * Shows a dialog. One for if a first and last name isn't entered.
	 * Also uses dialog for debugging.
	 */
    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        AlertDialog.Builder builder;
        Calendar c;
        switch(id){
		case DIALOG_SELECT_PERSON:
			builder = new AlertDialog.Builder(this);
			builder.setTitle("Select a person");
			builder.setItems(names.toArray(new CharSequence[people.size()]), new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int item) {
			        selectedName=names.get(item);
			        selectedPerson=people.get(item);
			        loadCases();
			        updateView();
			    }
			});
			dialog=builder.create();
			break;
		case DIALOG_SELECT_CASE:
			builder = new AlertDialog.Builder(this);
			builder.setTitle("Select one");
			tempActivity=this;
			builder.setItems(R.array.case_operations, new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int item) {
			        item+=SET_REMINDER;
			    	switch(item){
			        case(SET_REMINDER)://Set a reminder
			        	showDialog(DIALOG_SELECT_REMINDER);
			        	break;
			        case(SELECT_EDIT_CASE)://Edit case data
			        	Intent i=new Intent(tempActivity,EditCase.class);
				    	Bundle bundle=new Bundle();
				    	bundle.putString("caseNum", selectedCase.getCaseNum());
				    	bundle.putString("date", selectedCase.getDate());
				    	bundle.putString("desc", selectedCase.getDesc());
				    	bundle.putInt("id", selectedPerson.getID());
				    	i.putExtras(bundle);
				    	startActivityForResult(i,EDIT_CASE);
			        	break;
			        case(SELECT_DELETE_CASE)://Delete case data
			        	cch.deleteCase(selectedCase.getCaseNum());
			        	loadCases();
			        	updateView();
			        	break;
			        }
			    }
			});
			dialog=builder.create();
			break;
		case DIALOG_SELECT_REMINDER:
			builder = new AlertDialog.Builder(this);
			builder.setTitle("Select a Reminder Type");
			builder.setItems(R.array.reminder_types, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					sItem=item;
					showDialog(SELECT_DATE);
					//showDialog(SELECT_TIME);
			    	//insertCalendar(item);
			    }
			});
			dialog=builder.create();
			break;
		case SELECT_DATE:
	        c=Calendar.getInstance();
	        mYear=c.get(Calendar.YEAR);
	        mMonth=c.get(Calendar.MONTH);
	        mDay=c.get(Calendar.DAY_OF_MONTH);
	        return new DatePickerDialog(this,mDateSetListener,mYear, mMonth, mDay);
		case SELECT_TIME:
	        c=Calendar.getInstance();
	        mHour=c.get(Calendar.HOUR);
	        mMinute=c.get(Calendar.MINUTE);
	        mAMPM=c.get(Calendar.AM_PM);
	        return new TimePickerDialog(this,mTimeSetListener,mHour, mMinute,false);
		case DEBUG:
			builder = new AlertDialog.Builder(this);
			String temp;
			if(mAMPM==Calendar.AM)temp=" AM";
			else temp=" PM";
			builder.setMessage(""+mHour+":"+mMinute+temp)
			       .setCancelable(false)
			       .setNeutralButton("Close", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			                dialog.cancel();
			           }
			       });
			dialog = builder.create();
			break;
		default:
		    dialog = null;
		}
		return dialog;
	}
    
    /**
     * Override the back button functionality. Sets it to menu.
     */
    public void onBackPressed()  
    {         
    	this.startActivity(new Intent(this,CCActivity.class));  
    }
    
    // the callback received when the user sets the date in the dialog
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, 
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    showDialog(SELECT_TIME);
                }
          };
          
    // the callback received when the user "sets" the time in the dialog
    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
    		new TimePickerDialog.OnTimeSetListener() {
				public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					mHour=hourOfDay;
					mMinute=minute;
					if(mHour>=12){
						mAMPM=Calendar.PM;
						mHour%=12;
					}
					insertCalendar();
				}
          };
}
