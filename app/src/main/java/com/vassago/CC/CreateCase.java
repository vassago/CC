package com.vassago.CC;

import java.util.Calendar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;


public class CreateCase extends Activity{
	private final int DEBUG=0,DIALOG_CASE_NULL=1,DIALOG_DATE=2;
	private String selectedPerson,caseDesc,caseNum;
	private int mYear,mMonth,mDay,id;
	private Button caseSelectButton;
	
	/** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_case);
        Bundle extras=getIntent().getExtras();
        selectedPerson=extras.getString("Name");
        id=extras.getInt("id");
        //Gets and sets button for case date entry. Defaults zero for m/d/y
        caseSelectButton=(Button)findViewById(R.id.setCaseDate);
        caseSelectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DIALOG_DATE);
            }
        });
        Calendar c=Calendar.getInstance();
        mYear=c.get(Calendar.YEAR);
        mMonth=c.get(Calendar.MONTH);
        mDay=c.get(Calendar.DAY_OF_MONTH);
    }
	/**
	 * Shows a dialog. One for if a first and last name isn't entered.
	 * Also uses dialog for debugging.
	 */
    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        AlertDialog.Builder builder;
        switch(id) {
        case DIALOG_CASE_NULL:
        	builder = new AlertDialog.Builder(this);
        	builder.setMessage("Please enter a case number, description, and date.")
        	       .setCancelable(false)
        	       .setNeutralButton("Close", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                dialog.cancel();
        	           }
        	       });
        	dialog = builder.create();
            break;
        case DIALOG_DATE:
        	return new DatePickerDialog(this,mDateSetListener,mYear, mMonth, mDay);
        case DEBUG:
        	builder = new AlertDialog.Builder(this);
        	builder.setMessage(selectedPerson)
        	       .setCancelable(false)
        	       .setNeutralButton("Close", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                dialog.cancel();
        	           }
        	       });
        	dialog = builder.create();
        	break;
        default:
            dialog = null;
        }
        return dialog;
    }
	
	/**
	 * Puts entered data into a bundle to be returned to the calling process
	 * @param button
	 */
	public void createCase(View button){
		CCHelper cch=CCHelper.getCCH();
		EditText cNum = (EditText) findViewById(R.id.caseNumField);  
		EditText cDesc = (EditText) findViewById(R.id.caseDescField);
		caseNum= cNum.getText().toString();  
		caseDesc = cDesc.getText().toString();
		if(caseNum.equals("")||caseDesc.equals("")){
			showDialog(DIALOG_CASE_NULL);
			setContentView(R.layout.create_case);
			return;
		}
		//showDialog(DEBUG);
		StringBuilder date=new StringBuilder();
		date.append((mMonth+1)).append("/").append(mDay).append("/").append(mYear);
		cch.insertCase(caseNum, date.toString(), caseDesc, id);
		Bundle bundle=new Bundle();
		bundle.putString(CCHelper.C_CASE, caseNum);
		bundle.putString(CCHelper.C_DESC, caseDesc);
		bundle.putString(CCHelper.C_DATE, date.toString());
		bundle.putString("Name", selectedPerson);
		Intent i = new Intent();
		i.putExtras(bundle);
		setResult(RESULT_OK, i);
		finish();
	}
    /**
     * Override the back button functionality. Sets it to menu.
     */
    public void onBackPressed()  
    {         
    	this.startActivity(new Intent(this,CCActivity.class));  
    }
    // the callback received when the user sets the date in the dialog
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, 
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                }
          };
}
